import pygame
import random
from groq_ai import get_ai_direction
from logger import log_game_state, log_ai_response

# Initialize Pygame
pygame.init()

# Set up the game window
WIDTH, HEIGHT = 640, 480
GRID_SIZE = 20
GRID_WIDTH = WIDTH // GRID_SIZE
GRID_HEIGHT = HEIGHT // GRID_SIZE
SCREEN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Snake AI Game")

# Colors
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)

# Snake initial position and direction
snake = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
direction = (0, -1)  # Start moving up

# Generate initial fruit position
fruit = (random.randint(0, GRID_WIDTH - 1), random.randint(0, GRID_HEIGHT - 1))

def draw_grid():
    for x in range(0, WIDTH, GRID_SIZE):
        pygame.draw.line(SCREEN, WHITE, (x, 0), (x, HEIGHT))
    for y in range(0, HEIGHT, GRID_SIZE):
        pygame.draw.line(SCREEN, WHITE, (0, y), (WIDTH, y))

def draw_snake():
    for segment in snake:
        pygame.draw.rect(SCREEN, GREEN, (segment[0] * GRID_SIZE, segment[1] * GRID_SIZE, GRID_SIZE, GRID_SIZE))

def draw_fruit():
    pygame.draw.rect(SCREEN, RED, (fruit[0] * GRID_SIZE, fruit[1] * GRID_SIZE, GRID_SIZE, GRID_SIZE))

def move_snake(new_direction):
    global direction, fruit
    
    # Update direction based on AI input
    if new_direction == "UP" and direction != (0, 1):
        direction = (0, -1)
    elif new_direction == "DOWN" and direction != (0, -1):
        direction = (0, 1)
    elif new_direction == "LEFT" and direction != (1, 0):
        direction = (-1, 0)
    elif new_direction == "RIGHT" and direction != (-1, 0):
        direction = (1, 0)
    
    new_head = ((snake[0][0] + direction[0]) % GRID_WIDTH, (snake[0][1] + direction[1]) % GRID_HEIGHT)
    snake.insert(0, new_head)
    
    if new_head == fruit:
        fruit = (random.randint(0, GRID_WIDTH - 1), random.randint(0, GRID_HEIGHT - 1))
    else:
        snake.pop()

def main():
    clock = pygame.time.Clock()
    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # Prepare game state for AI
        game_state = {
            "grid_size": (GRID_WIDTH, GRID_HEIGHT),
            "snake": snake,
            "fruit": fruit
        }

        # Log game state
        log_game_state(game_state)

        # Get AI direction
        ai_direction = get_ai_direction(game_state)

        # Log AI response
        log_ai_response(ai_direction)

        # Move snake based on AI direction
        move_snake(ai_direction)

        # Draw everything
        SCREEN.fill(BLACK)
        draw_grid()
        draw_snake()
        draw_fruit()
        pygame.display.flip()

        # Control game speed
        clock.tick(5)  # 5 FPS for easier visualization

    pygame.quit()

if __name__ == "__main__":
    main()
