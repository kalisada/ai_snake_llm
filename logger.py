import logging

# Configure logging
logging.basicConfig(level=logging.INFO, 
                    format='%(asctime)s - %(message)s', 
                    datefmt='%Y-%m-%d %H:%M:%S',
                    handlers=[
                        logging.FileHandler("snake_ai_game.log"),
                        logging.StreamHandler()
                    ])

def log_game_state(game_state):
    logging.info(f"Game State: {game_state}")

def log_ai_response(ai_direction):
    logging.info(f"AI Direction: {ai_direction}")
