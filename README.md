# Ai Snake Llm



Dobrý den,
dle vašeho zadání zasílám vypracované řešení. Níže najdete prompty a výsledné kódy:

## Prompty


```
create new snake Ai game. We will be using GROQ, here is code to use to communicate:

import os

from groq import Groq

client = Groq(
    api_key=os.environ.get("GROQ_API_KEY"),
)

chat_completion = client.chat.completions.create(
    messages=[
        {
            "role": "user",
            "content": "Explain the importance of fast language models",
        }
    ],
    model="llama3-8b-8192",
)

print(chat_completion.choices[0].message.content)
```

```
Implemented console logging and improved AI's decision-making to better target the fruit.
```
