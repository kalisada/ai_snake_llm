import os
from groq import Groq

client = Groq(
    api_key=os.environ.get("GROQ_API_KEY"),
)

def get_ai_direction(game_state):
    # Extract game state information
    grid_width, grid_height = game_state['grid_size']
    snake_head = game_state['snake'][0]
    fruit = game_state['fruit']

    # Calculate the difference between snake head and fruit
    dx = fruit[0] - snake_head[0]
    dy = fruit[1] - snake_head[1]

    # Determine the best direction to move
    if abs(dx) > abs(dy):
        best_move = "RIGHT" if dx > 0 else "LEFT"
    else:
        best_move = "DOWN" if dy > 0 else "UP"

    # Create a more detailed prompt for the AI
    prompt = f"""
    You are controlling a snake in a game. The current game state is:
    Grid size: {grid_width}x{grid_height}
    Snake head position: {snake_head}
    Fruit position: {fruit}
    
    The suggested best move to reach the fruit is: {best_move}
    
    Consider the following:
    1. Move towards the fruit
    2. Avoid hitting the walls (the grid wraps around)
    3. Avoid hitting the snake's own body
    
    Based on this information, what direction should the snake move next?
    Respond with only one word: UP, DOWN, LEFT, or RIGHT.
    """

    # Get AI response
    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "user",
                "content": prompt,
            }
        ],
        model="llama3-8b-8192",
    )

    # Extract and return the AI's decision
    ai_decision = chat_completion.choices[0].message.content.strip().upper()
    return ai_decision if ai_decision in ["UP", "DOWN", "LEFT", "RIGHT"] else best_move
